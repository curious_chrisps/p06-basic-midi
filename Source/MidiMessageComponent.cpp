//
//  MidiMessageComponent.cpp
//  JuceBasicWindow - App
//
//  Created by Christoph Schick on 07/11/2017.
//

#include "MidiMessageComponent.hpp"

MidiMessageComponent::MidiMessageComponent()
{
    
    addAndMakeVisible(messageTypeComboBox);
    for (int i = 1; i < 8; i++)
    {
        messageTypeComboBox.addItem(midiMessageType[i-1], i);
    }
    messageTypeComboBox.setSelectedId(1);
    
    addAndMakeVisible(messageTypeLabel);
    messageTypeLabel.attachToComponent(&messageTypeComboBox, false);
    messageTypeLabel.setJustificationType(juce::Justification::left);
    messageTypeLabel.setText("Message Type", dontSendNotification);
    
    addAndMakeVisible(channelLabel);
    channelLabel.attachToComponent(&channelIncDec, false);
    channelLabel.setJustificationType(juce::Justification::left);
    channelLabel.setText("Channel", dontSendNotification);
    
    addAndMakeVisible(numberLabel);
    numberLabel.attachToComponent(&numberIncDec, false);
    numberLabel.setJustificationType(juce::Justification::left);
    numberLabel.setText("Number", dontSendNotification);
    
    addAndMakeVisible(velocityLabel);
    velocityLabel.attachToComponent(&velocityIncDec, false);
    velocityLabel.setJustificationType(juce::Justification::left);
    velocityLabel.setText("Velocity", dontSendNotification);
    
    channelIncDec.setSliderStyle(Slider::IncDecButtons);
    channelIncDec.setTextBoxStyle(Slider::TextBoxLeft, false, 100, 20);
    addAndMakeVisible(channelIncDec);
    channelIncDec.setRange(1, 16, 1);
    
    numberIncDec.setSliderStyle(Slider::IncDecButtons);
    numberIncDec.setTextBoxStyle(Slider::TextBoxLeft, false, 100, 20);
    addAndMakeVisible(numberIncDec);
    numberIncDec.setIncDecButtonsMode (Slider::incDecButtonsDraggable_Vertical);
    numberIncDec.setRange(0, 127, 1);
    
    velocityIncDec.setSliderStyle(Slider::IncDecButtons);
    velocityIncDec.setTextBoxStyle(Slider::TextBoxLeft, false, 100, 20);
    addAndMakeVisible(velocityIncDec);
    velocityIncDec.setIncDecButtonsMode (Slider::incDecButtonsDraggable_Vertical);
    velocityIncDec.setRange(0, 127, 1);
    
}


MidiMessageComponent::~MidiMessageComponent()
{
}

void MidiMessageComponent::resized()
{
    messageTypeComboBox.setBounds(5, getHeight()/2, getWidth()/4 - 10, getHeight()/2);
    channelIncDec.setBounds(getWidth()/4 + 5, getHeight()/2, getWidth()/4 - 10, getHeight()/2);
    numberIncDec.setBounds((getWidth()/4) * 2, getHeight()/2, getWidth()/4 - 10, getHeight()/2);
    velocityIncDec.setBounds((getWidth()/4) * 3, getHeight()/2, getWidth()/4 - 10, getHeight()/2);
}

MidiMessage MidiMessageComponent::getMidiMessage()
{
    int channel = channelIncDec.getValue();
    int number = numberIncDec.getValue();
    uint8 velocity = velocityIncDec.getValue();
    
    switch(messageTypeComboBox.getSelectedId())
    {
        case 1:
            theMidiMessage = MidiMessage::noteOn(channel, number, velocity);
            break;
        case 2:
            theMidiMessage = MidiMessage::noteOff(channel, number, velocity);
            break;
        case 3:
            theMidiMessage = MidiMessage::programChange(channel, number);
            break;
        case 4:
            theMidiMessage = MidiMessage::pitchWheel(channel, number*129);
            break;
        case 5:
            theMidiMessage = MidiMessage::aftertouchChange(channel, number, velocity);
            break;
        case 6:
            theMidiMessage = MidiMessage::channelPressureChange(channel, velocity);
            break;
        case 7:
            theMidiMessage = MidiMessage::controllerEvent(channel, number, velocity);
            break;
        default:
            break;
    }
    
    return theMidiMessage;
}
