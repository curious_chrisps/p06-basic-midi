/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include <cstdint>


//==============================================================================
MainComponent::MainComponent()
{
    setSize (900, 400);
    
    // Set to VMPK Output because I am using a virtual keyboard.
    // Set the string to the words you find under "Source" after "From" in your Midi Monitor app.
    
    audioDeviceManager.setDefaultMidiOutput("VMPK Input");
    
    addAndMakeVisible(midiMessageComponent);
    
    addAndMakeVisible(sendButton);
    sendButton.setButtonText("Send");
    sendButton.addListener(this);
}

MainComponent::~MainComponent()
{
}

void MainComponent::resized()
{
    sendButton.setBounds(getWidth()-getWidth()/5, 20, getWidth()/5, 20);
    midiMessageComponent.setBounds(10, 10, (getWidth()/5)*4, 40);
}

void MainComponent::buttonClicked(Button* button)
{
    if (&sendButton == button)
    {
        MidiMessage hereWeGo = midiMessageComponent.getMidiMessage();
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(hereWeGo);
    }
    
}

void MainComponent::handleIncomingMidiMessage (MidiInput* input, const MidiMessage& message)
{
    DBG("MIDI Messge received!");
    

}


