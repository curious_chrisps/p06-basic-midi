//
//  MidiMessageComponent.hpp
//  JuceBasicWindow - App
//
//  Created by Christoph Schick on 07/11/2017.
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

class MidiMessageComponent  :   public Component
{
public:
//    =========+========+=======+========+=======+=======+=======+========+=
    MidiMessageComponent();
    ~MidiMessageComponent();
    
    void resized() override;
    
    /** Returns the selected MIDI message. */
    MidiMessage getMidiMessage();
    
private:
//    =========+========+=======+========+=======+=======+=======+========+=

    ComboBox    messageTypeComboBox;
    Label       messageTypeLabel;
    Slider      channelIncDec;
    Label       channelLabel;
    Slider      numberIncDec;
    Label       numberLabel;
    Slider      velocityIncDec;
    Label       velocityLabel;
    
    String midiMessageType[7] = {
        "Note On",
        "Note Off",
        "Program Change",
        "Pitch Bend Change",
        "Aftertouch",
        "Channel Pressure",
        "Control Change",
    };
    
    MidiMessage theMidiMessage;
};

